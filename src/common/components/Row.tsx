import React, {useEffect, useState} from 'react';
import {Swiper, SwiperSlide} from 'swiper/react';
import {Navigation} from 'swiper';
import 'swiper/scss';
import 'swiper/scss/navigation';
import 'swiper/scss/pagination';
import {Link} from 'react-router-dom';
// import './style.scss';
interface Props{
    title:string,
    fetchUrl: any
}
const Row :React.FC<Props> = (props) => {
  const {title, fetchUrl} = props;
  const baseUrlImg = 'https://image.tmdb.org/t/p/original';
  const [movies, setMovies] = useState([]);

  useEffect(()=>{
    fetchUrl
        .then((movie:any) => setMovies(movie.results));
  }, [setMovies]);
  return (
    <div className="movies__container">
      <div className="slider__preview">
        <h2>{title}</h2>
        <Swiper
          modules={[Navigation]}
          spaceBetween={20}
          slidesPerView={6}
          navigation

        >
          {
            movies.map((movie:any, index)=> (
              <SwiperSlide key={index}>
                <Link to={`/movie/${movie.id}`} key={movie.id} >
                  <img src={`${baseUrlImg}/${movie?.backdrop_path}`} alt=""/>
                </Link>
              </SwiperSlide>
            ))

          }

        </Swiper>
      </div>
    </div>
  );
};

export default Row;
