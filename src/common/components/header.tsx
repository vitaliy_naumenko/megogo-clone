import React from 'react';

const Header: React.FC = () => {
  return (
    <>
      <header>
        <div className="header__logo">
          <a href="/">
            <img src="/logo.png" alt="logo"/>
          </a>
        </div>
      </header>
    </>
  );
};

export default Header;
