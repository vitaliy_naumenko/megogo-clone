export interface Movie{
    title:string,
    backdrop_path:string,
    overview: string,
    original_language: string,
    vote_average: number,
    name:string,
    original_title:string


}
