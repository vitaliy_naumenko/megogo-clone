import React from 'react';
import HomeSlider from './HomeSlider';
import Header from '../../common/components/header';
import Row from '../../common/components/Row';
import MoviesServices from '../../services/APIServices';

const HomePage: React.FC = () => {
  return (
    <>
      <Header/>
      <HomeSlider/>
      <Row title="MEGOGO originals" fetchUrl={MoviesServices.fetchNetflixOriginals()}/>
      <Row title="Top rated on MEGOGO" fetchUrl={MoviesServices.fetchTopRated()}/>
      <Row title="Action movies" fetchUrl={MoviesServices.fetchActionMovies()}/>
      <Row title="Comedy movies" fetchUrl={MoviesServices.fetchActionMovies()}/>
      <Row title="Horror movies" fetchUrl={MoviesServices.fetchHorrorMovies()}/>
      <Row title="Romance movies" fetchUrl={MoviesServices.fetchRomanceMovies()}/>
      <Row title="Documentaries movies" fetchUrl={MoviesServices.fetchDocumentaries()}/>
    </>

  );
};

export default HomePage;
